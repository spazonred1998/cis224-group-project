width = 640;
height = 480;

var setup = function() {
   background(255,255,127);
};

//Functions to run the game.
var main = function() {
    totalGuesses = totalGuesses + 1;
    console.log("Total guesses: " + totalGuesses);
    console.log(computerNumber);
    console.log(userGuess);

    if(userGuess > computerNumber){
        output.innerHTML ="That's too high. You have made "+totalGuesses+" guesses";
    }
    else if(userGuess < computerNumber){
        output.innerHTML ="That's too low. You have made "+totalGuesses+" guesses";
    }
    else if(userGuess === computerNumber){
        output.innerHTML ="You got it in "+totalGuesses+" guesses!";
    }
    
    if (totalGuesses>=5){
        output.innerHTML ="You have used your 5 guesses. The number is "+computerNumber;
        guessBtn.disabled=true;
    }
}

guessBtn.onclick = function() {
    main();
};

setup();
