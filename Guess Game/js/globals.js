var computerNumber = Math.floor(Math.random() * 50) + 1;
var userGuess=0;
var totalGuesses = 0;

var input, output, guessBtn;

window.onload = function() {
    console.log("Document loaded");
    input = document.getElementById("input");
    guessBtn = document.getElementById("guessBtn");
    output = document.getElementById("output");
    
    input.onkeyup = function() {
        userGuess = parseInt(input.value);
    };
};